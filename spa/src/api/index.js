import axios from 'axios'

const getY = async () => {
  const y = await axios.get('gety')
  return y.data.temperature
}
const getObjectState = async () => {
  const result = await axios.get('is_power')
  console.log(result.data['power is'])
  return result.data['power is']
}
const getControllerState = async () => {
  const result = await axios.get('is_controller')
  console.log(result.data['controller is '])
  return result.data['controller is ']
}

const getX = async () => {
  const x = await axios.get('getx')
  return x.data.angle
}
const setX = async (x) => {
  await axios.get(`setx/${parseInt(x)}`)
}
const setY = async (y) => {
  await axios.get(`sety/${y.toFixed(4)}`)
}
const powerOn = async () => {
  await axios.get(`power_on`)
}
const powerOff = async () => {
  await axios.get(`power_off`)
}

const resetToInit = async () => {
  await axios.get(`reset`)
}
const switchController = async (controllerMode) => {
  await axios.get(`switch_controller/${controllerMode}`)
}

export {
  getY,
  getX,
  setX,
  setY,
  resetToInit,
  powerOn,
  powerOff,
  getObjectState,
  switchController,
  getControllerState
}
