import time
TIME_STEP = 0.00001      # This is h

steps_of_object = 0.0
currentTemperature = [0]  # This is y (current value for response)
currentAngle = [0]       # This is x
newAngle = 0
vector_y = [0.0, 0.0, 0.0]  # y1 y2 y3
power = False


def switch_power():
    global power
    power = not power


def get_power_mod():
    global power
    return power


def reset_object():
    global newAngle
    newAngle = 0
    currentTemperature[0] = 0
    currentAngle[0] = 0
    vector_y[0] = 0
    vector_y[1] = 0
    vector_y[2] = 0


def execute_object(temperature):
    global newAngle
    global steps_of_object
    while True:
        # time.sleep(0.001) #UNCOMMENT FOR REALTIME
        if get_power_mod():
            if newAngle != 0:
                currentAngle[0] = newAngle
                newAngle = 0
            y1, y2, y3 = next_step(vector_y[0], vector_y[1], vector_y[2])
            vector_y[0] = y1
            vector_y[1] = y2
            vector_y[2] = y3
            temperature[0] = y1
            steps_of_object += TIME_STEP


def next_step(y1_previous, y2_previous, y3_previous):
    new_y3 = y3_previous + TIME_STEP * (0.17 * currentAngle[0] - 60.31 * y3_previous -
                                        21.48 * y2_previous - y1_previous) / 89.42
    new_y2 = y2_previous + TIME_STEP * new_y3
    new_y1 = y1_previous + TIME_STEP * new_y2
    return new_y1, new_y2, new_y3
