import main_object

CONTROLLER_STEP = 0.0001
error_of_controller = 0.0001
trueTemperature = 0
is_good_temperature = True
mod = True
controller_function = None
action_thread = None
start = False

previous_step_count = 0

errorTemperature = 0  # нужная температоруа - текующая = разность(ошибка)
last_errorTemperature = 0
currentAngle = 0  # currentAngle = PreviouseAngle + 22.003 * errorTemperature - 21.977 * PreviousError
previousAngle = 0

get_true_angle_f = None


def reset_controller():
    global trueTemperature
    global previous_step_count
    global errorTemperature
    global last_errorTemperature
    global currentAngle
    global previousAngle
    trueTemperature = 0
    previous_step_count = 0
    errorTemperature = 0
    last_errorTemperature = 0
    currentAngle = 0
    previousAngle = 0


#получаение нового нормированного угла открытия крана
def get_true_angle():
    global mod
    def analog_controller():
        global previousAngle
        global errorTemperature
        global last_errorTemperature
        global currentAngle
        currentAngle = previousAngle + 22.003 * errorTemperature - 21.977 * last_errorTemperature
        if currentAngle < 0:
            currentAngle = 0
        if currentAngle > 360:
            currentAngle = 360
        last_errorTemperature = errorTemperature
        previousAngle = currentAngle

    def polinomial_controller():
        global previousAngle
        global errorTemperature
        global last_errorTemperature
        global currentAngle
        #U - выход регулятора, k - номер шага, E - Ошибка
        #U[kT] = U[(k - 1)T] + 17.09E[kT] - 16.9E[(k - 1)T]
        currentAngle = previousAngle + 17.48 * errorTemperature - 17.454 * last_errorTemperature
        if currentAngle < 0:
            currentAngle = 0
        if currentAngle > 360:
            currentAngle = 360
        last_errorTemperature = errorTemperature
        previousAngle = currentAngle
    if mod is True:
        return analog_controller
    else:
        return polinomial_controller


#установка новой требуемой температуры
def set_new_temperature(new_t):
    global trueTemperature
    global is_good_temperature
    trueTemperature = new_t
    is_good_temperature = False


#Основная функция для отдельного потока, которя занимается настройкой температуры - Регулятор
def set_this_temperature(current_temperature):
    global trueTemperature
    global errorTemperature
    global is_good_temperature
    global controller_function
    global error_of_controller
    trueTemperature = 0
    controller_function = get_true_angle()
    while True:
        if not is_good_temperature:
            if main_object.steps_of_object > CONTROLLER_STEP:
                main_object.steps_of_object = 0.0
                controller_function()
                main_object.newAngle = currentAngle
                errorTemperature = trueTemperature - current_temperature[0]
               # print('temperature: ' + current_temperature[0] + ', angle: ' + currentAngle)
            if abs(current_temperature[0] - trueTemperature) < error_of_controller:
                is_good_temperature = True
