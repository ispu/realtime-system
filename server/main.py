import threading
import main_object
import controller
import server_v02

myObject = None
myController = None
srv = None


if __name__ == "__main__":
    myObject = threading.Thread(target=main_object.execute_object, args=(main_object.currentTemperature,))
    myController = threading.Thread(target=controller.set_this_temperature, args=(main_object.currentTemperature,))
    myObject.daemon = True
    myController.daemon = True
    myObject.start()
    myController.start()
    srv = server_v02.APIServerClass()
    srv.app.run('0.0.0.0', 4000)
