from klein import Klein
import main_object
import controller
import json


class APIServerClass(object):
    app = Klein()

    def __init__(self):
        self.x = {}

    @app.route('/power_on', methods=['GET'])
    def start_object(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        if not main_object.get_power_mod():
            main_object.switch_power()
        return json.dumps({'power is': main_object.get_power_mod(), 'answer': 'ok'})

    @app.route('/power_off', methods=['GET'])
    def stop_object(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        if main_object.get_power_mod():
            main_object.switch_power()
        return json.dumps({'power is': main_object.get_power_mod(), 'answer': 'ok'})

    @app.route('/is_power', methods=['GET'])
    def is_object(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        if main_object.get_power_mod():
            return json.dumps({'power is': main_object.get_power_mod(), 'answer': 'ok'})
        else:
            return json.dumps({'power is': main_object.get_power_mod(), 'answer': 'ok'})

    @app.route('/reset', methods=['GET'])
    def restart(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        main_object.reset_object()
        controller.reset_controller()
        return json.dumps({'answer': 'ok'})

    @app.route('/gety', methods=['GET'])
    def get_y(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        return json.dumps({'temperature': main_object.currentTemperature[0], 'answer': 'ok'})

    @app.route('/sety/<float:y>', methods=['GET'])
    def set_y(self, request, y):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        controller.set_new_temperature(y)
        return json.dumps({'answer': 'ok'})

    @app.route('/setx/<int:x>', methods=['GET'])
    def set_x(self, request, x):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        main_object.newAngle = x
        return json.dumps({'answer': 'ok'})

    @app.route('/stop_controller', methods=['GET'])
    def stop_controller(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        controller.is_good_temperature = True
        controller.reset_controller()
        return json.dumps({'answer': 'ok'})

    # true аналоговый, false полиноминальный
    @app.route('/switch_controller/<string:switcher>', methods=['GET'])
    def switch_controller(self, request, switcher):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        controller.reset_controller()
        if str(switcher) == 'true':
            controller.mod = True
            controller.controller_function = controller.get_true_angle()
            return json.dumps({'controller is': True, 'answer': 'ok'})
        elif str(switcher) == 'false':
            controller.mod = False
            controller.controller_function = controller.get_true_angle()
            return json.dumps({'controller is': False, 'answer': 'ok'})
        else:
            return json.dumps({'answer': 'bad'})

    @app.route('/is_controller', methods=['GET'])
    def is_controller(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-Type', 'application/json')
        if controller.mod:
            return json.dumps({'controller is ': True, 'answer': 'ok'})
        else:
            return json.dumps({'controller is ': False, 'answer': 'ok'})

    @app.route('/getx', methods=['GET'])
    def get_x(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-type', 'application/json')
        return json.dumps({'angle': main_object.currentAngle[0], 'answer': 'ok'})

    @app.route('/get_info', methods=['GET'])
    def get_info(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Content-type', 'application/json')
        return json.dumps({'temperature': main_object.currentTemperature[0], 'angle': main_object.currentAngle[0], 'answer': 'ok'})
